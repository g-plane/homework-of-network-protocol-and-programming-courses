#include-once
#include <AutoItConstants.au3>
#include <FileConstants.au3>
#include <MsgBoxConstants.au3>
#include <StringConstants.au3>
#include <String.au3>

Func OnExit()
	TCPShutdown()
EndFunc   ;==>OnExit

OnAutoItExitRegister("OnExit")

Global $iPort = 4399
Global Const $bEOF = Binary(@CRLF & "{EOF}"), $iEOFLen = BinaryLen($bEOF), $sEOT = Chr(4), $sSTX = Chr(2)

Func ReceiveFile($iSocket, $sPath, $bShowMessage = False)
	Local $hFile = FileOpen($sPath, BitOR($FO_BINARY, $FO_OVERWRITE))
	Local $bData = Binary("")
	Local $iDataLen = 0
	Local $bEOFReached = False

	Do
		$bData = TCPRecv($iSocket, 4096, $TCP_DATA_BINARY)

		If @error Then
			If $bShowMessage Then
				MsgBox($MB_ICONERROR, Default, "文件接收失败")
			EndIf
			FileClose($hFile)
			FileDelete($sPath)
			Return False
		EndIf

		$iDataLen = BinaryLen($bData)
		If $iDataLen = 0 Then ContinueLoop

		If BinaryMid($bData, 1 + $iDataLen - $iEOFLen, $iEOFLen) = $bEOF Then
			$bData = BinaryMid($bData, 1, $iDataLen - $iEOFLen)
			$bEOFReached = True
		EndIf

		FileWrite($hFile, $bData)
	Until $bEOFReached

	FileClose($hFile)

	Return True
EndFunc   ;==>ReceiveFile

Func SendFile($iSocket, $sPath, $bShowMessage = False)
	Local $iFileSize = FileGetSize($sPath)
	Local $hFile = FileOpen($sPath, $FO_BINARY)
	Local $iOffset = 0

	Do
		FileSetPos($hFile, $iOffset, $FILE_BEGIN)
		TCPSend($iSocket, FileRead($hFile, 4096))

		If @error Then
			If $bShowMessage Then
				MsgBox($MB_ICONERROR, Default, "文件发送失败")
			EndIf
			FileClose($hFile)
			Return False
		EndIf

		$iOffset += 4096
	Until $iOffset >= $iFileSize

	FileClose($hFile)

	TCPSend($iSocket, $bEOF)

	Return True
EndFunc   ;==>SendFile

Func ReceiveLongString($iSocket)
	Local $sContent = "", $bEOTReached = False
	Do
		Local $bReceived = TCPRecv($iSocket, 8, $TCP_DATA_BINARY)
		If @error Then Return
		If BinaryLen($bReceived) = 0 Then ContinueLoop
		Local $sReceived = BinaryToString($bReceived, $SB_UTF8)
		If StringRight($sReceived, 1) == $sEOT Then
			$sReceived = StringTrimRight($sReceived, 1)
			$bEOTReached = True
		EndIf
		$sContent &= $sReceived
	Until $bEOTReached
	Return _HexToString($sContent)
EndFunc   ;==>ReceiveLongString

Func SendLongString($iSocket, $sContent)
	TCPSend($iSocket, StringToBinary(_StringToHex($sContent) & $sEOT, $SB_UTF8))
	Return True
EndFunc   ;==>SendLongString
